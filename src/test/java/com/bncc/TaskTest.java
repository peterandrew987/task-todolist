package com.bncc;

import com.bncc.model.Task;
import com.bncc.model.TaskStatus;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testGetTask() {
        int id = 5;
        String name = "Belajar TDD";
        TaskStatus status = TaskStatus.NOT_DONE;
        String category = "Learning";
        Task task = new Task(id, name, status, category);
        String expectedResult = "5. Belajar TDD [NOT DONE] Learning";
        assertEquals(expectedResult, task.toString());
    }

    @Test
    public void testGetTaskOnNullReferences() {
        Task task = new Task();
        String expectedResult = "null references";
        assertEquals(expectedResult, task.toString());
    }

    @Test
    public void testChangeStatus() {
        Task task = new Task(1, "Belajar Java", TaskStatus.NOT_DONE, "Learning");
        String expectedResult = TaskStatus.DONE.toString();
        task.markAsDone();
        String result = task.getTaskStatus().toString();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testChangeStatusOnDoneStatus() {
        Task task = new Task(1, "Belajar Java", TaskStatus.DONE, "Learning");
        String expectedResult = TaskStatus.DONE.toString();
        task.markAsDone();
        String result = task.getTaskStatus().toString();
        assertEquals(expectedResult, result);
    }
}