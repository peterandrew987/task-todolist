package com.bncc;

import com.bncc.model.Task;
import com.bncc.model.TaskStatus;
import com.bncc.service.ToDoList;
import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void testInsertAndGetTask() {
        ToDoList todo = new ToDoList();
        int id = 1;
        String name = "Learn Java";
        String category = "Learning";
        TaskStatus status = TaskStatus.NOT_DONE;
        Task task = new Task(id, name, status, category);
        todo.insertTask(task);
        assertEquals(task, todo.getTask(id));
    }

    @Test
    public void testGetTodoList() {
        ToDoList todo = new ToDoList();
        todo.insertTask(new Task(1, "Do dishes", TaskStatus.DONE, "Learning"));
        todo.insertTask(new Task(2, "Learn Java", TaskStatus.NOT_DONE, "Learning"));
        todo.insertTask(new Task(3, "Learn TDD", TaskStatus.NOT_DONE, "Learning"));
        String expectedResult = "1. Do dishes [DONE] Learning\n2. Learn Java [NOT DONE] Learning\n3. Learn TDD [NOT DONE] Learning";
        assertEquals(expectedResult, todo.getAllTask());
    }

    @Test
    public void testGetTaskWithCategory() {
        ToDoList todo = new ToDoList();
        todo.insertTask(new Task(1, "Do dishes", TaskStatus.DONE, "Learning Java"));
        todo.insertTask(new Task(2, "Learn Java", TaskStatus.NOT_DONE, "Learning Android"));
        todo.insertTask(new Task(3, "Learn TDD", TaskStatus.NOT_DONE, "Learning Java"));
        String expectedResult = "1. Do dishes [DONE] Learning Java\n3. Learn TDD [NOT DONE] Learning Java";
        assertEquals(expectedResult, todo.getTaskWithCategory("Learning Java"));
    }

    @Test
    public void testDeleteOnNullReferences() {
        ToDoList todo = new ToDoList();
        Task task = new Task(1, "Do dishes", TaskStatus.DONE, "Learning Java");
        todo.insertTask(task);
        todo.deleteTaskById(2);
        assertEquals(task.toString(), todo.getAllTask());
    }

    @Test
    public void testDeleteById() {
        ToDoList todo = new ToDoList();
        todo.insertTask(new Task(1, "Do dishes", TaskStatus.DONE, "Learning Java"));
        todo.insertTask(new Task(2, "Learn Java", TaskStatus.NOT_DONE, "Learning Android"));
        todo.insertTask(new Task(3, "Learn TDD", TaskStatus.NOT_DONE, "Learning Java"));
        todo.deleteTaskById(2);
        assertNull(todo.getTask(2));
    }
}