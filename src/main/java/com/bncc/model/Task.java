package com.bncc.model;

import java.util.Objects;

public class Task {
    private int id;
    private String name;
    private TaskStatus status;
    private String category;

    @Override
    public String toString() {
        if (isTaskNotNull()) {
            return this.id + ". " + this.name + " " + this.status + " " + this.category;
        }
        return "null references";
    }

    public Task() {

    }

    public Task(int id, String name, TaskStatus status, String category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    private boolean isTaskNotNull() {
        return name != null;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return this.category;
    }

    public void markAsDone() {
        this.status = TaskStatus.DONE;
    }

    public TaskStatus getTaskStatus() {
        return this.status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(name, task.name) &&
                status == task.status &&
                Objects.equals(category, task.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, status, category);
    }
}