package com.bncc.model;

public enum TaskStatus {
    DONE("[DONE]"),
    NOT_DONE("[NOT DONE]");

    private String displayName;

    TaskStatus(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
