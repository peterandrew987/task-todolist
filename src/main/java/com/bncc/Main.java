package com.bncc;

import com.bncc.view.View;

public class Main {

    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }

}
