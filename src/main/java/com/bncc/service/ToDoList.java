package com.bncc.service;

import com.bncc.model.Task;
import java.util.HashMap;

public class ToDoList {
    private HashMap<Integer, Task> tasks = new HashMap<>();

    public ToDoList() {
    }

    public void insertTask(Task task) {
        tasks.put(task.getId(), task);
    }

    public Task getTask(int index) {
        return tasks.get(index);
    }

    public String getAllTask() {
        if (tasks.isEmpty()) {
            return "no task";
        }
        StringBuilder todolist = new StringBuilder();
        for (HashMap.Entry task : tasks.entrySet()) {
            todolist.append(task.getValue().toString());
            todolist.append("\n");
        }
        deleteLastEnter(todolist);
        return todolist.toString();
    }

    public void deleteTaskById(int id) {
        tasks.remove(id);
    }

    public void markTaskAsDone(int id) {
        tasks.get(id).markAsDone();
    }

    public String getTaskWithCategory(String category) {
        boolean isCategoryNull = true;
        StringBuilder todolist = new StringBuilder();
        for (HashMap.Entry<Integer, Task> task : this.tasks.entrySet()) {
            if (category.equalsIgnoreCase(task.getValue().getCategory())) {
                todolist.append(task.getValue().toString());
                todolist.append("\n");
                isCategoryNull = false;
            }
        }
        if (!isCategoryNull) {
            deleteLastEnter(todolist);
        }
        return isCategoryNull ? "no such category " : todolist.toString();
    }

    public boolean isEmpty() {
        return tasks.isEmpty();
    }
    
    private void deleteLastEnter(StringBuilder string) {
        string.delete(
                string.length() - 1,
                string.length()
        );
    }
}
