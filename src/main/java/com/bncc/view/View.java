package com.bncc.view;

import com.bncc.model.Task;
import com.bncc.model.TaskStatus;
import com.bncc.service.ToDoList;
import java.util.Scanner;

public class View {
    private ToDoList todo;
    private Scanner scan;

    public View() {
        todo = new ToDoList();
        scan = new Scanner(System.in);
    }

    public void showMenu() {
        int choice;

        do {
            menuMain();
            choice = scan.nextInt();
            scan.nextLine();
            menuManager(choice);
        } while (choice != 6);
    }

    private void menuManager(int choice) {
        switch (choice) {
            case 1:
                menuInsertTask();
                break;
            case 2:
                printAllTask();
                scan.nextLine();
                break;
            case 3:
                seeMenuByCategory();
                break;
            case 4:
                menuDeleteTask();
                break;
            case 5:
                menuChangeStatus();
                break;
            case 6:
                break;
            default:
                System.out.println("Your choice is not valid .... \n");
                break;
        }
    }

    private void menuMain() {
        System.out.println("Todo List");
        System.out.println("=======================");
        System.out.println("1. Insert task to list");
        System.out.println("2. See all task");
        System.out.println("3. See task by Category");
        System.out.println("4. Delete task by id");
        System.out.println("5. Update status");
        System.out.println("6. Exit");
        System.out.println("do number [1/6] : ");
    }

    private void menuChangeStatus() {
        if (todo.isEmpty()) {
            printAllTask();
            System.out.println("Choose Task id to change : ");
            int changeId = scan.nextInt();
            scan.nextLine();
            todo.markTaskAsDone(changeId);
            System.out.println("Task : " + todo.getTask(changeId).toString());
            System.out.println("Status Done.....");
            scan.nextLine();
        } else {
            System.out.println("Task is Empty");
            scan.nextLine();
        }
    }

    private void menuDeleteTask() {
        if (todo.isEmpty()) {
            printAllTask();
            System.out.println("Choose Task id to delete : ");
            int deleteId = scan.nextInt();
            scan.nextLine();
            todo.deleteTaskById(deleteId);
            System.out.println("delete Done.....");
            scan.nextLine();
        } else {
            System.out.println("Task is Empty");
            scan.nextLine();
        }
    }

    private void seeMenuByCategory() {
        System.out.println("Choose category to see : ");
        String category = scan.nextLine();
        System.out.println(todo.getTaskWithCategory(category));
        scan.nextLine();
    }

    private void menuInsertTask() {
        System.out.println("Insert new Task ");
        System.out.println("=====================");
        System.out.println("Task id : ");
        int id = scan.nextInt();
        scan.nextLine();
        System.out.println("Task name : ");
        String name = scan.nextLine();
        System.out.println("Task Category : ");
        String category = scan.nextLine();
        todo.insertTask(new Task(id, name, TaskStatus.NOT_DONE, category));
        System.out.println("Task have been insterted ....");
    }

    private void printAllTask() {
        System.out.println(todo.getAllTask());
    }
}
